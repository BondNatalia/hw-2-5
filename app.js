// 1 Метод об'єкту - це функція, яка знаходиться всередині об'єкту
//2 Будь-який тип
//3 Це означає, що коли змінній присвоюємо об'єкт, то у змінній зберігається не сам об'єкт,а посилання на нього


function createNewUser () {
    const newUser = {
        firstName: prompt("Введіть ім'я"),
        lastName: prompt("Введіть прізвище"),
        getLogin: function() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        }
    }
    console.log(newUser.getLogin());
    return newUser;
}
createNewUser();



function createUser () {
    const newUser = {};
    Object.defineProperty(newUser, "firstName",{
        value:prompt("Введіть ім'я"),
        writable: false,
        configurable: true
    } );
    Object.defineProperty(newUser, "lastName",{
        value:prompt("Введіть прізвище"),
        writable: false,
        configurable: true
    } );

newUser.setFirstName = function(newFName) {
        return Object.defineProperty(newUser, "firstName",{
            value:newFName,
        } );
    }
    newUser.setLastName = function(newLName) {
    return Object.defineProperty(newUser, "lastName", {
        value:newLName,
    })}
        return newUser;
    }

console.log(createUser());

let myUser = createUser(); // Создали объект
myUser.firstName = "TEST1"; // Попытались напрямую перезаписать имя на
// другое.
    myUser.lastName = "TEST2"; // Попытались напрямую перезаписать фамилию
// на другую.
console.log(myUser); // Не смотря на попытки перезаписать имя / фамилию -
// они остались старыми, так как writable: false
myUser.setFirstName("TEST1"); // Пытаемся перезаписать имя через метод
myUser.setLastName("TEST2"); // Пытаемся перезаписать фамилию через
// метод
console.log(myUser);
